# AI on the edge: Object Detection
Sources:
- NVDIA TensorRT is a framework developed by NVIDIA that allows AI application to run on NVIDIA Jetsons efficiently. Please read more about it on [link](https://developer.nvidia.com/tensorrt).
- Repository edge intelligence project [link](https://gitlab.com/wahyuutomo101/edge-intelligence-lpr)
- Tensor RT demo by JK Jung [link](https://github.com/jkjung-avt/tensorrt_demos)
- Yolo Model by pjreddy (Joseph Redmon) [link](https://pjreddie.com/darknet/yolo/).

Requirements:
- Jetson Xavier NX / Jetson Nano
- Monitor, mouse, and keyboard
- Web cam
- Knows basic linux command

Purpose of the document:
- Source code documentation
- Manual instruction on how to run the application



# Access the Project on Jetson Xavier NX
## Power up
1. Plug the power cable to the outlet.
2. Connect the device to a monitor via HDMI cable.
3. Plug keyboard and mouse to the board.
4. Plug webcam to the board.
5. Press power button on the Jetson Cover.
6. Login using `uname: pme, passwd: pmeitb`
7. Connect to the network using wifi or ethernet.

## Project structure
Open terminal and go to the project directory
```bash
$ cd ~/project
```
In this directory, there are several sub directories
```bash
$ ls
ai-edge  crowd-analytics-edge  darknet  jetson_nano  setup-app  test.py  tests  ups
```
important ones are, 
`ai-edge` is the main object detection application,
`setup-app` is a python based GUI to start the application

### ai-edge
This is the main application of the object detection and counter. 
```bash
ai-edge/
├── main.py
├── requirements.txt            --- dependency
├── tests/                      --- test suite
└── src/
    ├── configurations.txt      --- store information about counting line, server, camera id, and name
    ├── sample.jpg
    ├── api/                    --- api module
    ├── core/
        ├── periodic_thread.py  --- thread to  send data periodically
        └── state_machine.py
    ├── imgs/                   --- images for testing
    ├── object_detector/
        ├── IObjectDetector.py  --- abstract class for object detector
        ├── Yolov3.py           --- yolo object detector
        └── yolo/               --- yolo weight, config, variant, and converter
    ├── tracker/                --- tracker function
    └── utils/                  --- utility for camera and image manipulation
```
to run a demo of the detection, type the followng command
```bash
$ cd ~/project/ai-edge
$ python main.py --demo --video src/imgs/test1.mp4
```

![](https://i.imgur.com/fSE9b95.png)

</br>

the demo run object detection and counter application with default arguments, without connection to server and without camera. To see all possible flags type the following command

```bash
# ~/project/ai-edge

$ python main.py -h

usage: main.py [-h] [--image IMAGE] [--video VIDEO] [--video_looping]
               [--rtsp RTSP] [--rtsp_latency RTSP_LATENCY] [--usb USB]
               [--onboard ONBOARD] [--copy_frame] [--do_resize]
               [--width WIDTH] [--height HEIGHT] [-c CATEGORY_NUM] [-m MODEL]
               [--demo]

Capture and display live camera video, while doing real-time object detection
with TensorRT optimized YOLO model on Jetson

optional arguments:
  -h, --help                    show this help message and exit
  --image IMAGE                 image file name, e.g. dog.jpg
  --video VIDEO                 video file name, e.g. traffic.mp4
  --video_looping               loop around the video file [False]
  --rtsp RTSP                   RTSP H.264 stream, e.g. rtsp://admin:123456@192.168.1.64:554
  --rtsp_latency RTSP_LATENCY   RTSP latency in ms [200]
  --usb USB                     USB webcam device id (/dev/video?) [None]
  --onboard ONBOARD             Jetson onboard camera [None]
  --copy_frame                  copy video frame internally [False]
  --do_resize                   resize image/video [False]
  --width WIDTH                 image width [640]
  --height HEIGHT               image height [480]
  -c CATEGORY_NUM, 
  --category_num CATEGORY_NUM   number of object categories [8]
  -m MODEL, --model MODEL
            [yolov3|yolov3-tiny|yolov3-spp|yolov4|yolov4-tiny]-[{dimension}],
            where dimension could be a single number
            (e.g. 288, 416, 608) or WxH (e.g. 416x256)
  --demo                        use application in demo mode
```
</br>

for instance, to run the application using webcam, with yolov4 and 8 object classes, run the follwing command

```bash
# ~/project/ai-edge

$ python main.py --demo -m yolov4-416 -c 8 --usb 0
```
:::warning
:warning: If you do not have the model in the right format `.trt`, you will find the following error:
</br>
```bash
...
with open(TRTbin, 'rb') as f, trt.Runtime(self.trt_logger) as runtime:
FileNotFoundError: [Errno 2] No such file or directory:
'/home/pme/project/ai-edge/src/object_detector/yolo/yolov4-416.trt'
...
```
:::
</br>

To handle such error, we need to build the TensorRT engine (*.trt) from our AI model.
:::warning
:bulb: The way mentioned here is originally from jkjung [github](https://github.com/jkjung-avt/tensorrt_demos). Please visit his github for more detailed explanation.
:::

Go to `yolo` sub directory under `object_detector`

```bash
$ cd ~/project/ai-edge/src/object_detector/yolo/

# ~/project/ai-edge/src/object_detector/yolo/
$ ls
download_yolo.sh        yolov3-416.onnx          yolov4-416.cfg
__init__.py             yolov3-416.trt           yolov4-416.onnx
old-yolov4-416.cfg      yolov3-416.weights       yolov4-416.trt
old-yolov4-416.weights  yolov3.cfg               yolov4-416.weights
onnx_to_tensorrt.py     yolov3-tiny-416.cfg      yolov4.cfg
plugins                 yolov3-tiny-416.onnx     yolov4-tiny-416.cfg
plugins.py              yolov3-tiny-416.trt      yolov4-tiny-416.weights
__pycache__             yolov3-tiny-416.weights  yolov4-tiny.cfg
requirements.txt        yolov3-tiny.cfg          yolov4-tiny.weights
yolo_to_onnx.py         yolov3-tiny.weights      yolov4.weights
yolov3-416.cfg          yolov3.weights           yolo_with_plugins.py
```
In this directory, there are many files associated with Yolo models. If these files do not present, download it using the `download_yolo.sh` script.
```bash
# ~/project/ai-edge/src/object_detector/yolo/

$ ./download_yolo.sh
```
After downloading, there are two kind of files that we need to know,
`*.cfg` defines the configuration of a neural network model for **darknet** framework
`*.weights` defines the weight of a neural network model after training the model using **darknet**

</br>

Before building the TensorRT engine, we need to convert the model that we want to build from darknet format to ONNX format.
> ONNX is an open format built to represent machine learning models. [read more](https://onnx.ai/) [color=blue]

```bash
# ~/project/ai-edge/src/object_detector/yolo/

$ python yolo_to_onnx.py -c 8 -m yolov4-416

...
Checking ONNX model...
Saving ONNX file...
Done.
```

Upon successful, we will have `yolov4-416.onnx` file. Now we can build the TensorRT engine using this ONNX file.

```bash
# ~/project/ai-edge/src/object_detector/yolo/

$ python onnx_to_tensorrt.py -c 8 -m yolov4-416

cls num: 8
Loading ONNX file from path yolov4-416.onnx...
Adding yolo_layer plugins...
Building an engine.  This would take a while...
(Use "--verbose" to enable verbose logging.)
Completed creating engine.
Serialized the TensorRT engine to file: yolov4-416.trt
```
Once the process finish, we will have `yolov4-416.trt` file, which can be utilized to run the object detection and counter application.

:::warning
:warning: Please keep in  mind that in this project, we only use Yolo models. Hence, we use the already provided `yolo_to_onnx.py` and `onnx_to_tensorrt.py` scripts to build the TensorRT engine. For other models, please find alternative ways to do this (i.e., find scripts made by people or write it yourself).
:::

### setup-app
This is the graphical user interface to start the application developed using python with `tkinter` module. 
```bash
setup-app/
├── camera.py
├── check_wifi.py
├── config_manager.py
├── PageSetModel.py
├── PageSetServer.py
├── PageStart.py
├── PageSummary.py
├── PageTestCamera.py
├── SetupApp.py
├── StartPage.py
└── tegra_cam.py
```

to run the application type the following command.
:::warning
:warning:  to use this, a webcam and an available server are prerequisites. The application will not run with uncomplete system.
:::
```bash
$ cd ~/project/setup-app

# ~/project/setup-app
$ python SetupApp.py
```
![](https://i.imgur.com/QicSYcc.png)

