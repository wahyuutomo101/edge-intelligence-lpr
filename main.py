from src.core.state_machine import ObjectDetectionMachine

if __name__ == "__main__":
    fsm = ObjectDetectionMachine()
    fsm.initialize()
