import unittest
from pathlib import Path

from src.utils.config_manager import ConfigManager


class TestConfigManager(unittest.TestCase):
    def delete_config_file(self):
        p = Path(".")
        self.test_config_file = p / "tests" / "test_configurations.txt"

        if self.test_config_file.is_file():
            self.test_config_file.unlink()

    def test_init(self):
        self.delete_config_file()
        with open(self.test_config_file, "w+") as f:
            f.write("server=http://localhost:8000\n")

        cm = ConfigManager(self.test_config_file)
        self.assertEqual(cm.config_dict, {"server": "http://localhost:8000"})

    def test_init_no_config_file(self):
        with self.assertRaises(TypeError):
            cm = ConfigManager()  # noqa

    def test_update_config_dict(self):
        self.delete_config_file()
        with open(self.test_config_file, "w+") as f:
            f.write("server=http://localhost:8000\n")
            f.write("location=intersection\n")
            f.write("lines=0 0 1 1, 0 0 0.5 0.5")

        cm = ConfigManager(self.test_config_file)
        cm.update_config_dict()
        res_dict = {
            "server": "http://localhost:8000",
            "location": "intersection",
            "lines": "0 0 1 1, 0 0 0.5 0.5",
        }
        self.assertEqual(cm.config_dict, res_dict)

    def test_update_config_dict_empty(self):
        self.delete_config_file()
        with open(self.test_config_file, "w+") as f:
            f.write("")

        cm = ConfigManager(self.test_config_file)
        cm.update_config_dict()

        self.assertEqual(cm.config_dict, {})

    def test_update_config_add_new_field(self):
        p = Path(".")
        self.test_config_file = p / "tests" / "test_configurations.txt"

        cm = ConfigManager(self.test_config_file)
        cm.update_config("camera", "0001")
        cm.update_config("location", "here")

        self.assertEqual(cm.config_dict["camera"], "0001")

    def test_update_config_edit_existed_field(self):
        p = Path(".")
        self.test_config_file = p / "tests" / "test_configurations.txt"

        cm = ConfigManager(self.test_config_file)
        cm.update_config("camera", "0002")

        self.assertEqual(cm.config_dict["camera"], "0002")

    def test_get_counting_line(self):
        self.delete_config_file()
        with open(self.test_config_file, "w+") as f:
            f.write("server=http://localhost:8000\n")
            f.write("location=intersection\n")
            f.write("lines=0 0 1 1,0 0 0.5 0.5")

        cm = ConfigManager(self.test_config_file)
        cm.update_config_dict()
        expected_result = [(0.0, 0.0, 1.0, 1.0), (0.0, 0.0, 0.5, 0.5)]
        result = cm.get_counting_lines()

        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
