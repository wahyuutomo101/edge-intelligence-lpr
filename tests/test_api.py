import unittest
from unittest import mock

from src.api.api import Api

SERVER = "http://192.168.0.100:8000"


class TestApi(unittest.TestCase):
    def test_init(self):
        #  Check if server is properly assigned
        api = Api("http://localhost:8000")
        self.assertEqual(api.server, "http://localhost:8000")

    def test_init_server_none(self):
        #  Check response when conf is not given
        api = Api(None)
        self.assertEqual(api.server, "http://localhost:8000")

    def test_login(self):
        username = "admin"
        password = "bandung123abc"

        api = Api(SERVER)

        api.login = mock.MagicMock(return_value=({"token": "abcde"}, 200))

        response, status = api.login(username, password)

        self.assertEqual(status, 200)

    def test_login_failed(self):
        username = "admin"
        password = "admin"

        api = Api(SERVER)

        api.login = mock.MagicMock(return_value=({"token": "abcde"}, 400))

        response, status = api.login(username, password)

        self.assertEqual(status, 400)

    def test_register_camera(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        image = "/home/pme/project/crowd-analytics-edge/sample.jpg"

        api = Api(SERVER)
        api.login = mock.MagicMock(return_value=({"token": "abcde"}, None))
        api.register = mock.MagicMock(return_value=("response", 200))

        response, status = api.register(camera, image)
        self.assertEqual(status, 200)

    def test_register_empty_camera(self):
        camera = None
        image = "/home/pme/project/crowd-analytics-edge/sample.jpg"

        api = Api(SERVER)
        api.login = mock.MagicMock(return_value=({"token": "abcde"}, None))

        api.register = mock.MagicMock(return_value=("response", 200))

        response, status = api.register(camera, image)
        self.assertEqual(status, 200)

    def test_register_wrong_camera(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b2"
        image = "/home/pme/project/crowd-analytics-edge/sample.jpg"

        api = Api(SERVER)
        api.login = mock.MagicMock(return_value=({"token": "abcde"}, None))

        api.register = mock.MagicMock(return_value=("response", 200))

        response, status = api.register(camera, image)
        self.assertEqual(status, 200)

    def test_register_empty_image(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        image = None

        api = Api(SERVER)
        api.login = mock.MagicMock(return_value=({"token": "abcde"}, None))

        with self.assertRaises(TypeError):
            response, status = api.register(camera, image)

    def test_register_image_not_found(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        image = "./sample2.jpg"

        api = Api(SERVER)
        api.login = mock.MagicMock(return_value=({"token": "abcde"}, None))

        token = "abcde"

        with self.assertRaises(FileNotFoundError):
            response, status = api.register(token, camera, image)

    def test_get_settings(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        api = Api(SERVER)

        api.get_settings = mock.MagicMock(return_value=("response", 200))

        response, status = api.get_settings(camera)

        self.assertEqual(status, 200)

    def test_get_settings_empty_camera(self):
        camera = None
        api = Api(SERVER)

        api.get_settings = mock.MagicMock(return_value=("response", 500))

        response, status = api.get_settings(camera)

        self.assertEqual(status, 500)

    def test_get_settings_wrong_camera(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b5"
        api = Api(SERVER)

        api.get_settings = mock.MagicMock(return_value=("response", 400))

        response, status = api.get_settings(camera)

        self.assertEqual(status, 400)


if __name__ == "__main__":
    unittest.main()
