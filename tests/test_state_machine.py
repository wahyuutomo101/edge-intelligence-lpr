import unittest
from unittest import mock

from src.core.state_machine import ObjectDetectionMachine


class TestObjectDetectionMachine(unittest.TestCase):
    def test_register(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        with mock.patch("sys.argv", ["--model", "yolov3-416", "--usb", "0"]):
            # args = parse_args()
            fsm = ObjectDetectionMachine()
            fsm.args.model = "yolov3-416"
            fsm.args.usb = 0
            fsm.api.login = mock.MagicMock(return_value=({"token": "abcde"}, 200))
            fsm.api.register = mock.MagicMock(
                return_value=({"camera_serial": camera}, 200)
            )
            result = fsm.register(camera)

            self.assertTrue(result)

    def test_register_camera_not_found(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        with mock.patch("sys.argv", ["--model", "yolov3-416", "--usb", "1"]):
            # args = parse_args()
            fsm = ObjectDetectionMachine()
            fsm.args.model = "yolov3-416"
            fsm.args.usb = 1
            fsm.api.register = mock.MagicMock(
                return_value=({"camera_serial": camera}, 200)
            )
            with self.assertRaises(RuntimeError):
                result = fsm.register(camera)  # noqa

    def test_get_settings(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        with mock.patch("sys.argv", ["--model", "yolov3-416", "--usb", "0"]):
            # args = parse_args()
            fsm = ObjectDetectionMachine()
            fsm.args.model = "yolov3-416"
            fsm.args.usb = 0
            fsm.api.get_settings = mock.MagicMock(
                return_value=(
                    {"location": "Dago", "counting_lines": [[0.0, 0.0, 1, 1]]},
                    200,
                )
            )
            result = fsm.get_settings(camera)

            self.assertTrue(result)

    def test_get_settings_no_location(self):
        camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
        with mock.patch("sys.argv", ["--model", "yolov3-416", "--usb", "0"]):
            # args = parse_args()
            fsm = ObjectDetectionMachine()
            fsm.args.model = "yolov3-416"
            fsm.args.usb = 0
            fsm.api.get_settings = mock.MagicMock(
                return_value=(
                    {"location": None, "counting_lines": [[0.0, 0.0, 1, 1]]},
                    200,
                )
            )
            result = fsm.get_settings(camera)

            self.assertIsNone(result)


if __name__ == "__main__":
    unittest.main()
