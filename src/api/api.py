import requests


def apiPost(url, payload=None, json=None, token=None, files=None, timeout=60):
    headers = {}
    if token:
        headers["Authorization"] = "token " + token
    if json:
        headers["content-type"] = "application/json"

    print(f"headers: {headers}")

    try:
        #  print(f"headers: {headers}")
        response = requests.post(
            url=url,
            headers=headers,
            data=payload,
            json=json,
            files=files,
            timeout=timeout,
        )

        print(response)

        response.raise_for_status()

        return response.json(), response.status_code

    except requests.exceptions.Timeout as e:
        return e.response.text, e.response.status_code

    except requests.exceptions.InvalidSchema as e:
        return e.response.text, e.response.status_code

    except requests.exceptions.HTTPError as e:
        return e.response.text, e.response.status_code

    except Exception as e:
        print(e)
        return None, None


def apiGet(url, param=None, timeout=60):
    try:
        response = requests.get(url=url)
        response.raise_for_status()
        return response.json(), response.status_code

    except requests.exceptions.Timeout as e:
        return e.response.text, e.response.status_code

    except requests.exceptions.InvalidSchema as e:
        return e.response.text, e.response.status_code

    except requests.exceptions.HTTPError as e:
        return e.response.text, e.response.status_code

    except Exception as e:
        print(e)
        return None, None


class Api:
    def __init__(self, server=None):
        self.server = "http://localhost:8000"
        if server:
            self.server = server

    def register(self, token, camera=None, image=None):
        url = f"{self.server}/cameras/register/"

        try:
            image_file = open(image, "rb")

            files = {"media": image_file}
            payload = {"camera_serial": camera}

            response, status_code = apiPost(
                url=url, payload=payload, token=token, files=files
            )

            image_file.close()

            return response, status_code

        except FileNotFoundError:
            raise FileNotFoundError

        except TypeError:
            raise TypeError

    def get_settings(self, camera):
        # print(f"GET {self.server}/cameras/{camera}/settings/")
        url = f"{self.server}/cameras/{camera}/settings/"

        response, status_code = apiGet(url)

        return response, status_code

    def post_count_data(self, payload):
        url = f"{self.server}/people-count/add/"

        response, status_code = apiPost(url=url, payload=payload)

        return response, status_code

    def post_object_count(self, payload):
        url = f"{self.server}/object-count/"

        response = requests.post(
            url=url, json=payload, headers={"content-type": "application/json"}
        )
        return response.json(), response.status_code

        # response, status_code = apiPost(url=url, json=payload)

        # return response, status_code

    def login(self, username, password):
        url = f"{self.server}/login/"
        payload = {"username": username, "password": password}

        response, status_code = apiPost(url=url, payload=payload)

        return response, status_code


if __name__ == "__main__":
    server = "54ae732f-566a-4bc9-a884-4ecd817602b8"
    a = Api("http://192.168.0.100:8000")
    response, status = a.get_settings(server)

    camera = "54ae732f-566a-4bc9-a884-4ecd817602b1"
    image = "./sample2.jpg"

    # response, status = a.register(camera, image)
    payload = {
        "count": {"car": 10, "person": 10, "bus": 10},
        "location": "Sarijadi",
        "camera": "54ae732f-566a-4bc9-a884-4ecd817602b1",
        "timestamp": "2021-01-05 15:00:48.501835+07:00",
    }
    response, status = a.post_object_count(payload)

    print(f"status: {status}, response: {response}")
