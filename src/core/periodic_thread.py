import threading
import time
from datetime import datetime

from pytz import timezone


class ChildThread(threading.Thread):
    def __init__(self, threadID, name, delay, conf, api, clb):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay
        self.data = {}
        self.conf = conf
        self.api = api
        self.clb = clb

    def run(self):
        print("Starting " + self.name)
        tz = timezone("Asia/Jakarta")
        location = self.conf.get("location", "")
        camera = self.conf.get("camera", "")

        while True:
            timestamp = datetime.now(tz)
            payload = {
                "count": self.data,
                "location": location,
                "camera": camera,
                "timestamp": str(timestamp),
            }
            print(f"payload: {payload}")

            # self.api.post_count_data(payload)
            self.api.post_object_count(payload)
            self.clb()

            time.sleep(self.delay)

        print("Exiting " + self.name)

    def update_data(self, data):
        # print(f"update_data: {data}")
        self.data = data
        # print(f"after update: {self.data}")
