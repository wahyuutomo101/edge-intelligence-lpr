import argparse
import random
import time
from pathlib import Path

import cv2
import numpy as np
from statemachine import State, StateMachine

from src.api.api import Api
from src.core.periodic_thread import ChildThread
from src.object_detector.Yolov3 import Yolov3
from src.tracker.sort import Sort
from src.utils.camera import Camera, add_camera_args
from src.utils.config_manager import ConfigManager
from src.utils.display import open_window, set_display, show_fps
from src.utils.nms import check_overlap, nms, soft_nms  # noqa
from src.utils.visualization import BBoxVisualization
from src.utils.yolo_classes import get_cls_dict

WINDOW_NAME = "yolov3-tiny-custom-416"


def parse_args():
    """Parse input arguments."""
    desc = (
        "Capture and display live camera video, while doing "
        "real-time object detection with TensorRT optimized "
        "YOLO model on Jetson"
    )
    parser = argparse.ArgumentParser(description=desc)
    parser = add_camera_args(parser)
    parser.add_argument(
        "-c",
        "--category_num",
        type=int,
        default=8,
        help="number of object categories [80]",
    )
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        default="yolov3-416",
        help=(
            "[yolov3|yolov3-tiny|yolov3-spp|yolov4|yolov4-tiny]-"
            "[{dimension}], where dimension could be a single "
            "number (e.g. 288, 416, 608) or WxH (e.g. 416x256)"
        ),
    )
    parser.add_argument(
        "--demo",
        action='store_true',
        help="use application in demo mode"
    )
    args = parser.parse_args()
    return args


class ObjectDetectionMachine(StateMachine):
    init = State("Init", initial=True)
    setup = State("Setup")
    operation = State("Operation")
    error = State("Error")
    terminated = State("Terminated")

    initialize = init.to(setup)
    start = setup.to(operation)
    fail = operation.to(error)
    setup_fail = setup.to(error)
    recover = error.to(operation)
    reset = error.to(setup)
    terminate = operation.to(terminated)

    config_file = Path(__file__).parent.parent / "configurations.txt"
    conf = ConfigManager(config_file.absolute())
    api = Api(server=conf.get_server())

    args = parse_args()

    def on_enter_setup(self):
        if not self.args.demo:
            print("enter setup")
            registered = False
            camera = self.conf.get_camera()

            fail = False

            while not registered and not fail:
                try:
                    registered = self.register(camera)
                except Exception as e:
                    print(e)
                    fail = True
                    self.setup_fail()
                time.sleep(random.uniform(0, 10))

            setting_complete = False
            camera = self.conf.get_camera()

            while not setting_complete and not fail:
                try:
                    setting_complete = self.get_settings(camera)
                except Exception as e:
                    print(e)
                    fail = True
                    self.setup_fail()
                time.sleep(random.uniform(0, 10))

            if self.is_setup:
                self.start()
        else:
            print("demo mode")
            if self.is_setup:
                self.start()

    def on_enter_operation(self):
        print("enter operation")

        try:
            cam = Camera(self.args)
        except Exception as e:
            print(e)
            raise e

        cls_dict = get_cls_dict(self.args.category_num)
        vis = BBoxVisualization(cls_dict)

        self.model = Yolov3(self.args.model, 0.25, self.args.category_num)

        open_window(
            WINDOW_NAME,
            "Test",
            cam.img_width,
            cam.img_height,
        )

        self.object_counter = {}

        self.t1 = ChildThread(
            threadID=1,
            name="send_car_counter",
            delay=10,
            conf=self.conf.config_dict,
            api=self.api,
            clb=self.reset_counter,
        )
        if not self.args.demo:
            self.t1.start()

        self.run_detection(cam, vis)

        cam.release()
        cv2.destroyAllWindows()

    def on_enter_error(self):
        print("ERROR ERROR ERROR ERROR!!!")

    def on_enter_terminated(self):
        print("successfully terminated . . .")

    def on_start(self):
        print("starting...")

    def run_detection(self, cam, vis):
        try:
            img = cam.read()
            (frame_h, frame_w) = img.shape[:2]
        except Exception as e:
            print(e)

        tracker = Sort(max_age=3, min_hits=1)
        frame_count = 0
        skip_frame = 1

        full_scrn = False
        fps = 0.0
        tic = time.time()

        if not self.args.demo:
            points = self.conf.get_counting_lines()[0]
        else:
            points = (0, 0.5, 1, 0.5) # demo
        x1, y1, x2, y2 = points
        x1 = int(frame_w * x1)
        x2 = int(frame_w * x2)
        y1 = int(frame_h * y1)
        y2 = int(frame_h * y2)

        try:
            m = (y2 - y1) / (x2 - x1)
            c = y1 - m * x1
        except Exception as e:
            print(e)

        total_objects = 0

        cls_dict = get_cls_dict(self.args.category_num)

        limit = 150

        while True:
            # Check if the window is closed
            if cv2.getWindowProperty(WINDOW_NAME, 0) < 0:
                break

            frame_count += 1
            if frame_count % skip_frame != 0:
                continue

            # Read a frame from the camera
            try:
                img = cam.read()
            except Exception as e:
                print(e)

            try:
                (frame_h, frame_w) = img.shape[:2]
            except Exception as e:
                print(e)

            # Run detection
            boxes, confs, clss = self.model.detect(img)

            dets = []
            tracks = []
            keep = []

            keep = nms(boxes, 0.3, confs)
            # keep = soft_nms(boxes,confs,Nt=0.3,sigma=0.5,thresh=0.5,method=2)
            # no_overlap = check_overlap(boxes)

            # nms_res = [int(i[0]) for i in keep]
            nms_res = keep

            # final = set(nms_res).intersection(set(no_overlap))
            final = nms_res

            idx = 0
            for box, score, cl in zip(boxes, confs, clss):
                p0 = (
                    int(box[0] + (box[2] - box[0]) / 2),
                    int(box[1] + (box[3] - box[1]) / 2),
                )
                yp0 = m * p0[0] + c

                if (idx in final) and (abs(p0[1] - yp0) < limit):

                    dets.append([box[0], box[1], box[2], box[3], score, cl])

                idx += 1

            dets = np.asarray(dets)
            dets.reshape((dets.shape[0], 6))

            if dets.shape[0] == 0:
                dets = np.empty((0, 6))

            tracks = tracker.update(dets)  # noqa
            for trk in tracker.trackers:

                path = trk.track[-5:]
                for i in range(len(path) - 1):
                    cv2.line(img, path[i], path[i + 1], (255, 0, 100), 3)

                p1 = path[0]
                p0 = path[-1]

                yp0 = m * p0[0] + c
                yp1 = m * p1[0] + c

                try:
                    cls_name = cls_dict[trk.cls]
                except KeyError:
                    cls_name = "unknown"

                if (
                    (p0[1] < yp0)
                    and (p1[1] > yp1)
                    and (p0[0] > x1)
                    and (p1[0] < x2)
                    and (trk.counted is False)
                ):
                    if cls_name in self.object_counter:
                        self.object_counter[cls_name]["object_up"] += 1
                        self.object_counter[cls_name]["total_objects"] += 1
                    else:
                        self.object_counter[cls_name] = {
                            "object_up": 1,
                            "object_down": 0,
                            "total_objects": 1,
                        }
                    total_objects += 1
                    trk.counted = True
                elif (
                    (p0[1] > yp0)
                    and (p1[1] < yp1)
                    and (p0[0] > x1)
                    and (p1[0] < x2)
                    and (trk.counted is False)
                ):
                    if cls_name in self.object_counter:
                        self.object_counter[cls_name]["object_down"] += 1
                        self.object_counter[cls_name]["total_objects"] += 1
                    else:
                        self.object_counter[cls_name] = {
                            "object_up": 0,
                            "object_down": 1,
                            "total_objects": 1,
                        }
                    total_objects += 1
                    trk.counted = True

                #vis.draw_bboxes_trk(img, trk)

            k = 2
            ln = cv2.LINE_AA
            font = cv2.FONT_HERSHEY_PLAIN
            for key, value in self.object_counter.items():
                det_text = (
                    f"{key}: up:{value['object_up']}, down:{value['object_down']}"
                )
                cv2.putText(img, det_text, (11, k * 20), font, 1.0, (32, 32, 32), 4, ln)
                cv2.putText(
                    img, det_text, (10, k * 20), font, 1.0, (240, 240, 240), 1, ln
                )
                k += 1
            # Draw bounding boxes
            img = vis.draw_bboxes(img, boxes, confs, clss)

            # Draw lines
            # for line in  self.conf.get_counting_lines():
            if self.args.demo:
                line = ( 0, 0.5, 1, 0.5)
            else:
                line = self.conf.get_countinglines()[0]

            if line:
                x1, y1, x2, y2 = line
                x1 = int(frame_w * x1)
                x2 = int(frame_w * x2)
                y1 = int(frame_h * y1)
                y2 = int(frame_h * y2)

                cv2.line(img, (x1, y1), (x2, y2), (0, 255, 255), 4)
                cv2.line(img, (x1, y1), (x2, y2), (0, 255, 255), 4)
                cv2.line(img, (x1 - 45, y1 + limit), (x2, y2 + limit), (0, 255, 255), 2)
                cv2.line(img, (x1 + 30, y1 - limit), (x2, y2 - limit), (0, 255, 255), 2)
            # Draw fps
            img = show_fps(img, fps)

            # Show images
            cv2.imshow(WINDOW_NAME, img)

            # Calculate fps
            toc = time.time()
            curr_fps = 1.0 / (toc - tic)
            # calculate an exponentially decaying average of fps number
            fps = curr_fps if fps == 0.0 else (fps * 0.95 + curr_fps * 0.05)
            tic = toc

            # car_obj = self.object_counter.get("car", {})
            # car_count = car_obj.get("total_objects", 0)
            # print(f"car_count: {car_count}")

            obj_count = {
                k: v.get("total_objects", 0) for k, v in self.object_counter.items()
            }
            print(f"object count: {obj_count}")

            self.t1.update_data(obj_count)

            key = cv2.waitKey(1)
            if key == 27:  # ESC key: quit program
                break
            elif key == ord("F") or key == ord("f"):  # Toggle fullscreen
                full_scrn = not full_scrn
                set_display(WINDOW_NAME, full_scrn)

    def register(self, camera):
        print("register self to the server")
        try:
            cam = Camera(self.args)
        except Exception as e:
            print(e)
            raise e

        open_window(WINDOW_NAME, "capture an image", cam.img_width, cam.img_height)

        if cv2.getWindowProperty(WINDOW_NAME, 0) < 0:
            print("error window not open")

        image_name = "/home/pme/project/ai-edge/src/sample.jpg"

        img = cam.read()

        if img is None:
            raise RuntimeError("Failed to read frame")

        # cv2.imshow(WINDOW_NAME, img)
        # key = cv2.waitKey(1)
        cv2.imwrite(image_name, img)

        cam.release()

        try:
            resp, status = self.api.login("admin", "bandung123abc")
        except Exception as e:
            print(e)
            return None

        if status != 200:
            print("login failed")
            return None
        token = resp.get("token", None)

        response, status = self.api.register(token, camera, image_name)

        if status != 200:
            return None

        camera = response.get("camera_serial", None)

        if camera is not None:
            if camera == self.conf.get_camera():
                return True

            self.conf.update_config("camera", camera)

            print("successfully registered")
            return True

        return None

    def get_settings(self, camera):
        # print("get settings from the server")
        response, status = self.api.get_settings(camera)

        if status != 200:
            print(response)
            return None

        location = response.get("location", None)
        if location is not None:

            self.conf.update_config("location", location)

        counting_lines = response.get("counting_lines", [])
        lines = []

        for counting_line in counting_lines:
            lines.append(" ".join(map(str, counting_line)))

        self.conf.update_config("lines", ",".join(lines))

        if location and counting_lines:
            return True

        return None

    def reset_counter(self):
        for k, v in self.object_counter.items():
            v["total_objects"] = 0


if __name__ == "__main__":
    fsm = ObjectDetectionMachine()
    fsm.initialize()
