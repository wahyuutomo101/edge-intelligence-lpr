# Declare class for storing object position throughout frames
class ObjectPosition:
    def __init__(self, objectNumber, objectPosition):
        # Store new object number
        # Create list for storing object positions
        # Initialize objectPosition list with current position
        self.objectNumber = objectNumber
        self.objectPositions = [objectPosition]

        # Create variable for checking whether the object
        # has already been counted or not
        self.objectCounted = False

