# Import modules and libraries
import numpy as np
from collections import OrderedDict
from scipy.spatial import distance as dist

# Declare class for object tracking purpose
# Register object, update object position, deregister object
class ObjectTracker:
    def __init__(self, countMissed=50, countDistance=50):
        # Initialize new objectNumber for new object
        # Assign each objectNumber with dictionary for
        # Tracking object position and 
        # Counting the number of missing frame for each object
        self.newObjectNumber = 0
        self.objects = OrderedDict()
        self.objectMissed = OrderedDict()

        # Store value for maximum number of missed frame and distance
        self.maxMissed = countMissed
        self.maxDistance = countDistance
    
    def addObject(self, position):
        # Method for storing position and missed count for new object 
        self.objects[self.newObjectNumber] = position
        self.objectMissed[self.newObjectNumber] = 0

        # Increment object number for new next object index
        self.newObjectNumber += 1

    def removeObject(self, objectNumber):
        # Method for deleting object from objects dictionary
        del self.objects[objectNumber]
        del self.objectMissed[objectNumber]

    def updateObject(self, boundingBoxes):
        # Method for updating object position
        # Check whether the boundingBoxes list is empty or not
        # Condition for empty boundingBoxes
        if (len(boundingBoxes) == 0):
            # Loop over objectNumber and treat each object as missing object
            # By incrementing objectMissed value
            for objectNumber in list(self.objectMissed.keys()):
                self.objectMissed[objectNumber] += 1

                # Check whether the number of missed frame for object has passed maxMissed value
                # Remove object if it has passed the maxMissed value
                if (self.objectMissed[objectNumber] > self.maxMissed):
                    self.removeObject(objectNumber)

            # Return to main program since there are no object to update
            return self.objects
        
        # Condition for non empty boundingBoxes
        # Declare an array for object position in current frame
        newPositions = np.zeros((len(boundingBoxes), 2), dtype='int')

        # Loop for each object's bounding box in order to get object position
        for (i, (x1,y1,x2,y2)) in enumerate(boundingBoxes):
            # Calculate position from bounding box
            xPos = int((x1 + x2) / 2.0)
            yPos = int((y1 + y2) / 2.0)

            # Add positions to newPositions list
            newPositions[i] = (xPos, yPos)

        # Check if object tracking has been done before
        # If it's not then register new object
        if (len(self.objects) == 0):
            for i in range(0, len(newPositions)):
                self.addObject(newPositions[i])

        # Update object position by matching past position
        # With closest new position
        else:
            # Get currrent objectNumber with its corresponding position
            objectNumbers = list(self.objects.keys())
            objectPositions = list(self.objects.values())

            # Calculate euclidean distance between current position and new position
            euclidDist = dist.cdist(np.array(objectPositions), newPositions)

            # Match current object position and new object position by
            # finding the smallest value in each row and sort row from
            # smallest value to largest value
            rows = euclidDist.min(axis=1).argsort()

            # Find smallest value in each column and sort column
            # From smallest value to largest value
            cols = euclidDist.argmin(axis=1)[rows]

            # Create variables for storing used rows and cols after performing
            # add, remove, or update object
            usedRows = set()
            usedCols = set()

            # Loop over rows and cols tuples index
            for (row, col) in zip(rows, cols):
                # Check whether row or col has been used before
                # Skip if it's true
                if ((row in usedRows) or (col in usedCols)):
                    continue

                # Check whether the new distance is greater then maxDistance
                # If it's true then the new position is not correspond to old object
                if (euclidDist[row][col] > self.maxDistance):
                    continue

                # Else set new position for the object and reset objectMissed counter
                objectNumber = objectNumbers[row]
                self.objects[objectNumber] = newPositions[col]
                self.objectMissed[objectNumber] = 0

                # Add row and col to usedRows and usedCols list
                usedRows.add(row)
                usedCols.add(col)

            # Find unused euclidDist rows and columns
            unusedRows = set(range(0, euclidDist.shape[0])).difference(usedRows)
            unusedCols = set(range(0, euclidDist.shape[1])).difference(usedCols)

            # Check whether number of objectPositions greater than newPositions or vice versa
            # Case if number of objectPositions is greater than newPositions
            # Increment objectMissed value for un-updated object
            if (euclidDist.shape[0] >= euclidDist.shape[1]):
                # Loop over unused object
                for row in unusedRows:
                    # Get corresponding objectNumber
                    objectNumber = objectNumbers[row]
                    self.objectMissed[objectNumber] += 1

                    # Check whether the missed count for the object has passed maxMissed value
                    # If yes the remove the object
                    if (self.objectMissed[objectNumber] > self.maxMissed):
                        self.removeObject(objectNumber)

            # Case if number of newPositions is greater than objectPositions
            else:
                for col in unusedCols:
                    self.addObject(newPositions[col])
            
        # Return the objects
        return self.objects
        





