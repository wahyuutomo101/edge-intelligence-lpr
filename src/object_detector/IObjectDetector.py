from abc import ABC, abstractmethod


class IObjectDetector(ABC):
    
    @abstractmethod
    def detect(self, img):
        pass
