import pycuda.autoinit
from src.object_detector.yolo.yolo_with_plugins import TrtYOLO

from .IObjectDetector import IObjectDetector


class Yolov3(IObjectDetector):
    def __init__(self,model, conf_th, cls_number):
        self.conf_th = conf_th
        self.model = TrtYOLO(model, (416, 416), cls_number)

    def detect(self, img):
        return self.model.detect(img, self.conf_th)
