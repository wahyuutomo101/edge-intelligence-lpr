import re
from os import fdopen, remove
from shutil import copymode, move
from tempfile import mkstemp


class ConfigManager:
    def __init__(self, config_file):
        self.config_file = config_file
        self.config_dict = {}
        self.update_config_dict()

    def get_config_dict(self):
        return self.config_dict

    def get_server(self):
        return self.config_dict.get("server", None)

    def get_camera(self):
        return self.config_dict.get("camera", None)

    def get_location(self):
        return self.config_dict.get("location", None)

    def get_counting_lines(self):
        lines = self.config_dict.get("lines", [])
        parsed_lines = [tuple(map(float, line.split(" "))) for line in lines.split(",")]

        return parsed_lines

    def update_config_dict(self):
        with open(self.config_file) as f:
            conf_lines = f.readlines()
            temp_lines = list(map(lambda x: x.split("="), conf_lines))
            self.config_dict = {
                line[0]: line[1].rstrip("\n") for line in temp_lines if len(line) > 1
            }

    def update_config(self, pattern, new):
        with open(self.config_file, "r+") as f:
            data = f.read()
            field = re.search(pattern, data)

            if field is not None:
                self.replace(field.group(0), new)
            else:
                entry = f"{pattern}={new}\n"
                f.write(entry)

        self.update_config_dict()

    def replace(self, pattern, subst):
        # Create temp file
        file_path = self.config_file
        fh, abs_path = mkstemp()
        with fdopen(fh, "w") as new_file:
            with open(file_path) as old_file:
                for line in old_file:
                    old = f"{pattern}=.*"
                    new = f"{pattern}={subst}"

                    line = re.sub(old, new, line)
                    new_file.write(line)

        # Copy the file permissions from the old file to the new file
        copymode(file_path, abs_path)
        # Remove original file
        remove(file_path)
        # Move new file
        move(abs_path, file_path)
